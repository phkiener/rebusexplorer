﻿namespace RebusExplorer.Contracts
{
    using System;
    using System.Collections.Generic;

    public sealed class Message
    {
        public Message(
            long id,
            string type,
            int priority,
            DateTimeOffset expiration,
            DateTimeOffset visible,
            IReadOnlyList<Header> headers,
            string body)
        {
            Id = id;
            Type = type;
            Priority = priority;
            Expiration = expiration;
            Visible = visible;
            Headers = headers;
            Body = body;
        }

        public long Id { get; }
        public string Type { get; }
        public int Priority { get; }
        public DateTimeOffset Expiration { get; }
        public DateTimeOffset Visible { get; }
        public IReadOnlyList<Header> Headers { get; }
        public string Body { get; }

        public sealed class Header
        {
            public Header(string name, string value)
            {
                Name = name;
                Value = value;
            }

            public string Name { get; }
            public string Value { get; }
        }
    }
}
