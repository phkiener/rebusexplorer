namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;

    internal sealed class DropMessageAsserter : BaseAsserter<DropMessage>
    {
        protected override IValidation Validate(DropMessage value)
        {
            return Validator.Check().That(() => value.Connection).IsNotNull().IsValid();
        }
    }
}
