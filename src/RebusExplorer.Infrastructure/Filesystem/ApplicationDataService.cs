namespace RebusExplorer.Infrastructure.Filesystem
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Contracts;

    public class ApplicationDataService : IApplicationDataService
    {
        private const string PartSeparator = "|";
        private readonly ApplicationDataConfiguration configuration;

        public ApplicationDataService(ApplicationDataConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<IReadOnlyList<Connection>> GetRecentConnections()
        {
            EnsureConfigurationExists();
            var linesInFile = await File.ReadAllLinesAsync(configuration.RememberedConnectionsFilePath);

            return linesInFile.Select(ParseConnection).ToList();
        }

        public async Task StoreConnections(IReadOnlyList<Connection> connections)
        {
            EnsureConfigurationExists();
            var lines = connections.Select(SerializeConnection).ToList();

            await File.WriteAllLinesAsync(configuration.RememberedConnectionsFilePath, lines);
        }

        private void EnsureConfigurationExists()
        {
            if (!File.Exists(configuration.RememberedConnectionsFilePath))
            {
                var directoryName = Path.GetDirectoryName(configuration.RememberedConnectionsFilePath);
                Directory.CreateDirectory(directoryName);
                File.Create(configuration.RememberedConnectionsFilePath);
            }
        }

        private static Connection ParseConnection(string line)
        {
            var parts = line.Split(PartSeparator);

            if (parts.Length != 3)
            {
                throw new Exception($"Invalid connection format: {line}");
            }

            var connectionType = parts[0] switch
            {
                "SQL" => Connection.ConnectionType.SqlDirect,
                _ => throw new Exception($"Unknown connection type {parts[0]}")
            };

            return new Connection(parts[1], parts[2], connectionType);
        }

        private static string SerializeConnection(Connection connection)
        {
            var connectionMode = connection.Type switch
            {
                Connection.ConnectionType.SqlDirect => "SQL",
                _ => throw new Exception($"Unknown connection type {connection.Type}")
            };

            return string.Join(PartSeparator, connectionMode, connection.ConnectionString, connection.QueueName);
        }
    }
}
