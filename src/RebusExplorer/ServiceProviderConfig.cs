namespace RebusExplorer
{
    using System.Reflection;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;
    using Swallow.Validation.ServiceCollection;
    using Validation;

    public static class ServiceProviderConfig
    {
        public static IServiceCollection AddRebusExplorer(this IServiceCollection serviceCollection)
        {
            var assembly = Assembly.GetExecutingAssembly();

            return serviceCollection
                .AddValidationContainer(assembly)
                .AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
        }
    }
}
