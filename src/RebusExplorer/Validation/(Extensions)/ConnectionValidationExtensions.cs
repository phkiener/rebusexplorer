namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;

    internal static class ConnectionValidationExtensions
    {
        public static IAssertion<Connection> IsValid(this IAssertable<Connection> assertion)
        {
            return assertion.Satisfies(new ConnectionAsserter());
        }
    }
}
