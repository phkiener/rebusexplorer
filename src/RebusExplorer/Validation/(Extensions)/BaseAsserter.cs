namespace RebusExplorer.Validation
{
    using Swallow.Validation;
    using Swallow.Validation.Assertions.Errors;
    using Swallow.Validation.Errors;

    internal abstract class BaseAsserter<T> : IAsserter<T>
    {
        public bool Check(T value, out ValidationError error)
        {
            var result = Validate(value).Result();

            error = result.IsError ? new EntityValidationError(result.Errors) : null;
            return result.IsSuccess;
        }

        protected abstract IValidation Validate(T value);
    }
}
