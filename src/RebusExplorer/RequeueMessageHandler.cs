namespace RebusExplorer
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class RequeueMessageHandler : IRequestHandler<RequeueMessage>
    {
        private readonly IRebusMessageRepository messageRepository;

        public RequeueMessageHandler(IRebusMessageRepository messageRepository)
        {
            this.messageRepository = messageRepository;
        }

        public Task<Unit> Handle(RequeueMessage request, CancellationToken cancellationToken)
        {
            var connectionString = request.Connection.ConnectionString;
            var queue = request.Connection.QueueName;

            var message = messageRepository.GetMessage(connectionString, request.MessageId, queue);

            using (var transaction = messageRepository.BeginTransaction(connectionString))
            {
                try
                {
                    var targetQueue = message.Headers.Single(h => h.Name == "rbs2-return-address").Value;

                    messageRepository.AddMessage(connectionString, message, targetQueue);
                    messageRepository.RemoveMessage(connectionString, request.MessageId, queue);

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine($"Failed to requeue message {request.MessageId}: {e.Message}");
                    transaction.Rollback();
                }
            }

            return Unit.Task;
        }
    }
}
