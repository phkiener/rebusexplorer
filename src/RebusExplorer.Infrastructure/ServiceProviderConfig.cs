namespace RebusExplorer.Infrastructure
{
    using System;
    using Filesystem;
    using Microsoft.Extensions.DependencyInjection;
    using SqlDatabase;

    public static class ServiceProviderConfig
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IRebusMessageRepository, RebusMessageRepository>();
            serviceCollection.AddScoped<IApplicationDataService, ApplicationDataService>();

            var configurationPath = Environment.ExpandEnvironmentVariables(".");
            serviceCollection.AddSingleton(new ApplicationDataConfiguration($"{configurationPath}/connections.txt", 10));

            return serviceCollection;
        }
    }
}
