namespace RebusExplorer
{
    public interface IHasTransactions
    {
        ITransaction BeginTransaction(string connectionString);
        void EndTransaction();
    }
}
