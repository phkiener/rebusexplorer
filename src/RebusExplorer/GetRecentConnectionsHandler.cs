namespace RebusExplorer
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class GetRecentConnectionsHandler : IRequestHandler<GetRecentConnections, IReadOnlyList<Connection>>
    {
        private readonly IApplicationDataService applicationDataService;

        public GetRecentConnectionsHandler(IApplicationDataService applicationDataService)
        {
            this.applicationDataService = applicationDataService;
        }

        public async Task<IReadOnlyList<Connection>> Handle(GetRecentConnections request, CancellationToken cancellationToken)
        {
            return await applicationDataService.GetRecentConnections();
        }
    }
}
