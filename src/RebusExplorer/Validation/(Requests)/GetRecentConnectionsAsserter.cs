namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;

    internal sealed class GetRecentConnectionsAsserter : BaseAsserter<GetRecentConnections>
    {
        protected override IValidation Validate(GetRecentConnections value)
        {
            return Validator.Check();
        }
    }
}
