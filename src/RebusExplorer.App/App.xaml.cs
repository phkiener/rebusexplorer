﻿namespace RebusExplorer.App
{
    using System;
    using System.Windows;
    using Microsoft.Extensions.DependencyInjection;

    public partial class App
    {
        private IServiceProvider serviceProvider;

        public static T CreateWindow<T>() where T : Window
        {
            var application = Current;
            if (application is App current)
            {
                return current.serviceProvider.GetService<T>();
            }

            throw new InvalidOperationException();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            ConfigureServices();

            var launcher = CreateWindow<Launcher.Launcher>();
            launcher.Show();
        }

        private void ConfigureServices()
        {
            serviceProvider = new ServiceCollection()
                .AddServices()
                .AddWindows()
                .BuildServiceProvider();
        }
    }
}
