namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;

    internal sealed class RememberConnectionHandler : BaseAsserter<RememberConnection>
    {
        protected override IValidation Validate(RememberConnection value)
        {
            return Validator.Check().That(() => value.Connection).IsNotNull().IsValid();
        }
    }
}
