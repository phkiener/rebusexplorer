namespace RebusExplorer.Infrastructure.SqlDatabase
{
    using System;
    using System.Data.SqlClient;

    public class Transaction<TRepository> : ITransaction where TRepository : IHasTransactions
    {
        private readonly SqlTransaction sqlTransaction;
        private bool isWritten;
        private TRepository repository;

        public Transaction(TRepository repository, SqlTransaction sqlTransaction)
        {
            this.repository = repository;
            this.sqlTransaction = sqlTransaction;
        }

        public SqlConnection Connection => sqlTransaction.Connection;

        public void Commit()
        {
            if (isWritten)
            {
                throw new InvalidOperationException("Transaction was already written");
            }

            sqlTransaction.Commit();
            repository.EndTransaction();
            isWritten = true;
        }

        public void Rollback()
        {
            if (isWritten)
            {
                throw new InvalidOperationException("Transaction was already written");
            }

            sqlTransaction.Rollback();
            repository.EndTransaction();
            isWritten = false;
        }

        public void Dispose()
        {
            if (!isWritten)
            {
                Rollback();
            }

            sqlTransaction.Dispose();
        }

        public SqlCommand CreateCommand()
        {
            var command = sqlTransaction.Connection.CreateCommand();
            command.Transaction = sqlTransaction;

            return command;
        }
    }
}
