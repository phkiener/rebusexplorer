namespace RebusExplorer
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class CanConnectHandler : IRequestHandler<CanConnect, bool>
    {
        private readonly IRebusMessageRepository repository;

        public CanConnectHandler(IRebusMessageRepository repository)
        {
            this.repository = repository;
        }

        public Task<bool> Handle(CanConnect request, CancellationToken cancellationToken)
        {
            var connectionString = request.Connection.ConnectionString;

            try
            {
                using (var transaction = repository.BeginTransaction(connectionString))
                {
                    transaction.Commit();
                }

                return Task.FromResult(true);
            }
            catch (Exception)
            {
                return Task.FromResult(false);
            }
        }
    }
}
