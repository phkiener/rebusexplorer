namespace RebusExplorer
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class EditAndRequeueMessageHandler : IRequestHandler<EditAndRequeueMessage>
    {
        private readonly IRebusMessageRepository messageRepository;

        public EditAndRequeueMessageHandler(IRebusMessageRepository messageRepository)
        {
            this.messageRepository = messageRepository;
        }

        public Task<Unit> Handle(EditAndRequeueMessage request, CancellationToken cancellationToken)
        {
            var connectionString = request.Connection.ConnectionString;
            var queue = request.Connection.QueueName;

            var message = messageRepository.GetMessage(connectionString, request.MessageId, queue);

            using (var transaction = messageRepository.BeginTransaction(connectionString))
            {
                try
                {
                    var targetQueue = message.Headers.Single(h => h.Name == "rbs2-return-address").Value;
                    var modifiedMessage = new Message(
                        id: message.Id,
                        type: message.Type,
                        priority: message.Priority,
                        expiration: message.Expiration,
                        visible: message.Visible,
                        headers: message.Headers,
                        body: request.EditedBody);

                    messageRepository.AddMessage(connectionString, modifiedMessage, targetQueue);
                    messageRepository.RemoveMessage(connectionString, request.MessageId, queue);

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine($"Failed to requeue message {request.MessageId}: {e.Message}");
                    transaction.Rollback();
                }
            }

            return Unit.Task;
        }
    }
}
