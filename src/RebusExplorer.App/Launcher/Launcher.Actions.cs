namespace RebusExplorer.App.Launcher
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Common;
    using Contracts;
    using MediatR;

    public partial class Launcher
    {
        private readonly IMediator mediator;

        private async Task<IReadOnlyList<ConnectionViewModel>> LoadRecentConnections()
        {
            var connections = await mediator.Send(new GetRecentConnections());
            return connections.Select(MapToViewModel).ToList();
        }

        private async Task<bool> CheckConnection(ConnectionViewModel connectionViewModel)
        {
            var connection = MapToConnection(connectionViewModel);
            var checkCommand = new CanConnect(connection);
            try
            {
                return await mediator.SendInNewThreadAsync(checkCommand);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task RememberConnection(ConnectionViewModel connectionViewModel)
        {
            var connection = MapToConnection(connectionViewModel);
            var rememberCommand = new RememberConnection(connection);
            await mediator.SendInNewThreadAsync(rememberCommand);
        }

        private static Connection MapToConnection(ConnectionViewModel connectionViewModel)
        {
            return new Connection(
                connectionString: connectionViewModel.ConnectionString,
                queueName: connectionViewModel.QueueName,
                connectionType: Connection.ConnectionType.SqlDirect);
        }

        private static ConnectionViewModel MapToViewModel(Connection connection)
        {
            return new ConnectionViewModel
            {
                ConnectionString = connection.ConnectionString,
                QueueName = connection.QueueName,
                ConnectViaSql = connection.Type == Connection.ConnectionType.SqlDirect,
                ConnectViaRebus = false
            };
        }
    }
}
