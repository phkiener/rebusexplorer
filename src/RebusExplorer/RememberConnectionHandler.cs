namespace RebusExplorer
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class RememberConnectionHandler : IRequestHandler<RememberConnection>
    {
        private readonly IApplicationDataService applicationDataService;

        public RememberConnectionHandler(IApplicationDataService applicationDataService)
        {
            this.applicationDataService = applicationDataService;
        }

        public async Task<Unit> Handle(RememberConnection request, CancellationToken cancellationToken)
        {
            var connections = await applicationDataService.GetRecentConnections();

            var updatedConnections = connections
                .Where(c => !ConnectionsAreEqual(c, request.Connection))
                .Prepend(request.Connection)
                .ToList();

            await applicationDataService.StoreConnections(updatedConnections);
            return Unit.Value;
        }

        private static bool ConnectionsAreEqual(Connection x, Connection y)
        {
            if (x == null)
            {
                return y == null;
            }

            if (y == null)
            {
                return false;
            }

            if (ReferenceEquals(x, y))
            {
                return true;
            }

            return x.Type == y.Type && x.ConnectionString == y.ConnectionString && x.QueueName == y.QueueName;
        }
    }
}
