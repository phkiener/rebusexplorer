namespace RebusExplorer.App.MainWindow
{
    using System.Collections.Generic;

    public sealed class MessageViewModel
    {
        public MessageViewModel(long messageId, string body, string messageTitle, IReadOnlyList<MessageHeaderViewModel> headers)
        {
            MessageId = messageId;
            Body = body;
            MessageTitle = messageTitle;
            Headers = headers;
        }

        public long MessageId { get; }
        public string MessageTitle { get; }
        public string Body { get; }
        public IReadOnlyList<MessageHeaderViewModel> Headers { get; }
    }
}
