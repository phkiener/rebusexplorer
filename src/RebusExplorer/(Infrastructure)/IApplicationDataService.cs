namespace RebusExplorer
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Contracts;

    public interface IApplicationDataService
    {
        Task<IReadOnlyList<Connection>> GetRecentConnections();
        Task StoreConnections(IReadOnlyList<Connection> connections);
    }
}
