namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;

    internal sealed class EditAndRequeueMessageAsserter : BaseAsserter<EditAndRequeueMessage>
    {
        protected override IValidation Validate(EditAndRequeueMessage value)
        {
            return Validator.Check().That(() => value.Connection).IsNotNull().IsValid();
        }
    }
}
