namespace RebusExplorer.Contracts
{
    using MediatR;

    public class EditAndRequeueMessage : IRequest
    {
        public EditAndRequeueMessage(long messageId, string editedBody, Connection connection)
        {
            MessageId = messageId;
            EditedBody = editedBody;
            Connection = connection;
        }

        public long MessageId { get; }
        public string EditedBody { get; }
        public Connection Connection { get; }
    }
}
