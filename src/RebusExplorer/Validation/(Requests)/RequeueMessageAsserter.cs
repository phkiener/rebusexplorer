namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;

    internal sealed class RequeueMessageAsserter : BaseAsserter<RequeueMessage>
    {
        protected override IValidation Validate(RequeueMessage value)
        {
            return Validator.Check().That(() => value.Connection).IsNotNull().IsValid();
        }
    }
}
