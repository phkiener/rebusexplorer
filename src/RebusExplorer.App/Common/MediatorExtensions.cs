namespace RebusExplorer.App.Common
{
    using System.Threading.Tasks;
    using MediatR;

    internal static class MediatorExtensions
    {
        public static Task<TResponse> SendInNewThreadAsync<TResponse>(this IMediator mediator, IRequest<TResponse> request)
        {
            return Task.Run(() => mediator.Send(request));
        }
    }
}
