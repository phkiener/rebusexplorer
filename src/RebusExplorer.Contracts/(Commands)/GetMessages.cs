﻿namespace RebusExplorer.Contracts
{
    using System.Collections.Generic;
    using MediatR;

    public class GetMessages : IRequest<IReadOnlyList<Message>>
    {
        public GetMessages(Connection connection)
        {
            Connection = connection;
        }

        public Connection Connection { get; }
    }
}
