namespace RebusExplorer.Contracts
{
    using System.Collections.Generic;
    using MediatR;

    public sealed class GetRecentConnections : IRequest<IReadOnlyList<Connection>>
    {
    }
}
