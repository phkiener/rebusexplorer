namespace RebusExplorer
{
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class DropMessageHandler : IRequestHandler<DropMessage>
    {
        private readonly IRebusMessageRepository messageRepository;

        public DropMessageHandler(IRebusMessageRepository messageRepository)
        {
            this.messageRepository = messageRepository;
        }

        public Task<Unit> Handle(DropMessage request, CancellationToken cancellationToken)
        {
            var connectionString = request.Connection.ConnectionString;
            var queue = request.Connection.QueueName;

            messageRepository.RemoveMessage(connectionString, request.MessageId, queue);
            return Unit.Task;
        }
    }
}
