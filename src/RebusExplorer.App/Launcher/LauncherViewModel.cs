namespace RebusExplorer.App.Launcher
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    public sealed class LauncherViewModel : INotifyPropertyChanged
    {
        private bool isBusy;
        private ObservableCollection<ConnectionViewModel> rememberedConnections;
        private ConnectionViewModel selectedConnection;

        public LauncherViewModel()
        {
            rememberedConnections = new ObservableCollection<ConnectionViewModel>();
            selectedConnection = null;

            CurrentConnection = new ConnectionViewModel();
        }

        public ObservableCollection<ConnectionViewModel> RememberedConnections
        {
            get => rememberedConnections;
            set
            {
                rememberedConnections = value;
                PropertyHasChanged(nameof(RememberedConnections));
            }
        }

        public ConnectionViewModel SelectedConnection
        {
            get => selectedConnection;
            set
            {
                selectedConnection = value;
                PropertyHasChanged(nameof(SelectedConnection));
            }
        }

        public bool IsBusy
        {
            get => isBusy;
            set
            {
                isBusy = value;
                PropertyHasChanged(nameof(IsBusy));
                PropertyHasChanged(nameof(ConnectCanBeClicked));
                PropertyHasChanged(nameof(ConnectButtonText));
            }
        }

        public bool ConnectCanBeClicked => !IsBusy;
        public string ConnectButtonText => IsBusy ? "Connecting..." : "Connect";

        public ConnectionViewModel CurrentConnection { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void PropertyHasChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
