namespace RebusExplorer.App.MainWindow
{
    public class MessageHeaderViewModel
    {
        public MessageHeaderViewModel(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; }
        public string Value { get; }
    }
}
