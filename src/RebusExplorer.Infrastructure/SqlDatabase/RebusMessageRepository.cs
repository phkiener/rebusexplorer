﻿namespace RebusExplorer.Infrastructure.SqlDatabase
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using Contracts;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class RebusMessageRepository : IRebusMessageRepository
    {
        private Transaction<RebusMessageRepository> currentTransaction;

        public ITransaction BeginTransaction(string connectionString)
        {
            if (currentTransaction != null)
            {
                throw new InvalidOperationException("Cannot begin a transaction while another one is still open");
            }

            var connection = new SqlConnection(connectionString);
            connection.Open();

            var transaction = connection.BeginTransaction();
            currentTransaction = new Transaction<RebusMessageRepository>(this, transaction);

            return currentTransaction;
        }

        public void EndTransaction()
        {
            currentTransaction = null;
        }

        public IReadOnlyList<Message> GetMessages(string connectionString, string queue)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand())
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText =
                    $@"SELECT id, priority, expiration, visible, CAST(headers as varchar(max)) AS headers, CAST(body as varchar(max)) AS body FROM {queue}";

                var reader = command.ExecuteReader();
                var messages = new List<Message>();

                while (reader.Read())
                {
                    var message = ReadMessage(reader);
                    messages.Add(message);
                }

                reader.Close();
                reader.Dispose();

                return messages;
            }
        }

        public Message GetMessage(string connectionString, long messageId, string queue)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand())
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText =
                    $@"SELECT id, priority, expiration, visible, CAST(headers as varchar(max)) AS headers, CAST(body as varchar(max)) AS body FROM {queue} WHERE id = @id";
                command.Parameters.Add(new SqlParameter("@id", messageId));

                var reader = command.ExecuteReader();
                reader.Read();

                var message = ReadMessage(reader);
                reader.Close();
                reader.Dispose();

                return message;
            }
        }

        public void RemoveMessage(string connectionString, long messageId, string queue)
        {
            var command = BeginWrite(connectionString);
            command.CommandText = $@"DELETE FROM {queue} WHERE id = @id";
            command.Parameters.Add(new SqlParameter("@id", messageId));

            EndWrite(command);
        }

        public void AddMessage(string connectionString, Message message, string queue)
        {
            var command = BeginWrite(connectionString);
            command.CommandText =
                $@"INSERT INTO {queue} (priority, expiration, visible, headers, body) VALUES (@priority, @expiration, @visible, @headers, @body)";
            command.Parameters.Add(new SqlParameter("@priority", message.Priority));
            command.Parameters.Add(new SqlParameter("@expiration", message.Expiration));
            command.Parameters.Add(new SqlParameter("@visible", message.Visible));

            var rawHeaders = Encoding.UTF8.GetBytes(SerializeHeaders(message.Headers));
            var rawBody = Encoding.UTF8.GetBytes(message.Body);
            command.Parameters.Add(new SqlParameter("@headers", rawHeaders));
            command.Parameters.Add(new SqlParameter("@body", rawBody));

            EndWrite(command);
        }

        private static Message ReadMessage(SqlDataReader reader)
        {
            var id = reader.GetInt64(0);
            var priority = reader.GetInt32(1);
            var expiration = reader.GetDateTimeOffset(2);
            var visible = reader.GetDateTimeOffset(3);
            var headers = reader.GetString(4);
            var body = reader.GetString(5);

            var parsedHeaders = ParseHeaders(headers);
            var parsedType = ParseType(body);
            return new Message(id, parsedType, priority, expiration, visible, parsedHeaders, body);
        }

        private static string ParseType(string rawBody)
        {
            var json = JObject.Parse(rawBody);
            var typeValue = json.Property("$type")?.Value.ToString();

            return typeValue?.Substring(0, typeValue.IndexOf(',')) ?? string.Empty;
        }

        private static IReadOnlyList<Message.Header> ParseHeaders(string rawHeaders)
        {
            var json = JObject.Parse(rawHeaders);
            return json.Properties()
                .Select(p => new Message.Header(p.Name, p.Value.ToString()))
                .ToList();
        }

        private static string SerializeHeaders(IReadOnlyList<Message.Header> headers)
        {
            var json = new JObject();
            foreach (var header in headers)
            {
                json.Add(header.Name, new JValue(header.Value));
            }

            return json.ToString(Formatting.None);
        }

        private SqlCommand BeginWrite(string connectionString)
        {
            var command = currentTransaction != null
                ? currentTransaction.CreateCommand()
                : new SqlConnection(connectionString).CreateCommand();

            if (command.Connection.State != ConnectionState.Open)
            {
                command.Connection.Open();
            }

            return command;
        }

        private void EndWrite(SqlCommand command)
        {
            command.ExecuteNonQuery();

            if (currentTransaction == null)
            {
                command.Connection.Close();
                command.Connection.Dispose();
                command.Dispose();
            }
        }
    }
}
