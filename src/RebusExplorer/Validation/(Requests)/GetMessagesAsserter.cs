namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;

    internal sealed class GetMessagesAsserter : BaseAsserter<GetMessages>
    {
        protected override IValidation Validate(GetMessages value)
        {
            return Validator.Check().That(() => value.Connection).IsNotNull().IsValid();
        }
    }
}
