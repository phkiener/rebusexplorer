namespace RebusExplorer.Contracts
{
    using MediatR;

    public class RequeueMessage : IRequest
    {
        public RequeueMessage(long messageId, Connection connection)
        {
            MessageId = messageId;
            Connection = connection;
        }

        public long MessageId { get; }
        public Connection Connection { get; }
    }
}
