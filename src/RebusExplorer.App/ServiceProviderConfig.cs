namespace RebusExplorer.App
{
    using System.Reflection;
    using Infrastructure;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;

    public static class ServiceProviderConfig
    {
        public static IServiceCollection AddWindows(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddTransient<MainWindow.MainWindow>()
                .AddTransient<Launcher.Launcher>();
        }

        public static IServiceCollection AddServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddMediatR(Assembly.Load("RebusExplorer"))
                .AddRebusExplorer()
                .AddInfrastructure();
        }
    }
}
