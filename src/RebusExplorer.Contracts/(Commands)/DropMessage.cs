namespace RebusExplorer.Contracts
{
    using MediatR;

    public class DropMessage : IRequest
    {
        public DropMessage(long messageId, Connection connection)
        {
            MessageId = messageId;
            Connection = connection;
        }

        public long MessageId { get; }
        public Connection Connection { get; }
    }
}
