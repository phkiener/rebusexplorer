# RebusExplorer

A small GUI tool to take a peek at your Rebus error queue.
You can inspect failed messages, drop them completely or edit and requeue them.

As of now, it only definitely supports SqlServer as transport layer. Other SQL-transports probably
work as well, if they have the same database table format.

## The GUI

When you start the GUI, you will be greeted by a small launcher window asking you to provide a connection
string and the name of the queue to inspect. Enter you SqlServer connection string and bus name and click
"Connect", so that the main window will be opened.

In the main screen, you can click "Refresh messages" to get a list of all messages that are currently
in your selected queue. They will be listed on the left hand side with their respective ids; clicking
on one inspects it and lists the headers and values as well as its body on the right hand side.
Clicking on "Drop Message" removes the message from the queue, clicking on "Requeue message" puts it
in its source queue again. You can edit the body if you wish; it will be updated.

## Notes

This is still early development. I've hacked it together on a single weekend, and it works (with a
SqlServer Local DB instance, that is.) It's ugly - especially the GUI - but as a POC it's good enough.

It is not safe whatsoever. The error queue name technically allows SQL injection (as you can't parameterize
table names). Exceptions are caught and displayed in a message box.

## Licensing

RebusExplorer is licensed under the MIT license. That means you can do whatever you like with it, as long
as you give credit by including the license when distributing your software. 
