namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;
    using Swallow.Validation.Assertions.Errors;
    using Swallow.Validation.Errors;

    internal sealed class ConnectionAsserter : IAsserter<Connection>
    {
        public bool Check(Connection value, out ValidationError error)
        {
            var result = Validator.Check()
                .That(value.ConnectionString, nameof(value.ConnectionString)).IsNotNull().IsNotEmpty()
                .That(value.QueueName, nameof(value.QueueName)).IsNotNull().IsNotEmpty()
                .Result();

            error = result.IsError ? new EntityValidationError(result.Errors) : null;
            return result.IsSuccess;
        }
    }
}
