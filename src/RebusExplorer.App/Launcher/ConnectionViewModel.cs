namespace RebusExplorer.App.Launcher
{
    using System.ComponentModel;

    public sealed class ConnectionViewModel : INotifyPropertyChanged
    {
        private string connectionString;
        private bool connectViaRebus;
        private bool connectViaSql;
        private string queueName;

        public ConnectionViewModel()
        {
            connectionString = string.Empty;
            queueName = string.Empty;
            connectViaSql = true;
            connectViaRebus = false;
        }

        public string ConnectionString
        {
            get => connectionString;
            set
            {
                connectionString = value;
                PropertyHasChanged(nameof(ConnectionString));
            }
        }

        public string QueueName
        {
            get => queueName;
            set
            {
                queueName = value;
                PropertyHasChanged(nameof(QueueName));
            }
        }

        public bool ConnectViaSql
        {
            get => connectViaSql;
            set
            {
                connectViaSql = value;
                PropertyHasChanged(nameof(ConnectViaSql));
            }
        }

        public bool ConnectViaRebus
        {
            get => connectViaRebus;
            set
            {
                connectViaRebus = value;
                PropertyHasChanged(nameof(ConnectViaRebus));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void PropertyHasChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
