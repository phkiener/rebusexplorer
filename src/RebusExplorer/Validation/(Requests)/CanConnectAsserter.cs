namespace RebusExplorer.Validation
{
    using Contracts;
    using Swallow.Validation;
    using Swallow.Validation.Assertions;

    internal sealed class CanConnectAsserter : BaseAsserter<CanConnect>
    {
        protected override IValidation Validate(CanConnect value)
        {
            return Validator.Check().That(() => value.Connection).IsNotNull().IsValid();
        }
    }
}
