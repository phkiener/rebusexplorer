namespace RebusExplorer.Validation
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using Swallow.Validation;
    using Swallow.Validation.Exceptions;

    public sealed class RequestValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ValidationContainer validationContainer;

        public RequestValidationBehavior(ValidationContainer validationContainer)
        {
            this.validationContainer = validationContainer;
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var isValid = validationContainer.Check(request, out var error);
            if (!isValid)
            {
                throw new ValidationException(new[] { error });
            }

            cancellationToken.ThrowIfCancellationRequested();
            return next.Invoke();
        }
    }
}
