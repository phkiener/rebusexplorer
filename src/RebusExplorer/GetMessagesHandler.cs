namespace RebusExplorer
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Contracts;
    using MediatR;

    public class GetMessagesHandler : IRequestHandler<GetMessages, IReadOnlyList<Message>>
    {
        private readonly IRebusMessageRepository messageRepository;

        public GetMessagesHandler(IRebusMessageRepository messageRepository)
        {
            this.messageRepository = messageRepository;
        }

        public Task<IReadOnlyList<Message>> Handle(GetMessages request, CancellationToken cancellationToken)
        {
            var connectionString = request.Connection.ConnectionString;
            var queue = request.Connection.QueueName;

            return Task.FromResult(messageRepository.GetMessages(connectionString, queue));
        }
    }
}
