namespace RebusExplorer
{
    using System.Collections.Generic;
    using Contracts;

    public interface IRebusMessageRepository : IHasTransactions
    {
        IReadOnlyList<Message> GetMessages(string connectionString, string queue);
        Message GetMessage(string connectionString, long messageId, string queue);
        void RemoveMessage(string connectionString, long messageId, string queue);
        void AddMessage(string connectionString, Message message, string queue);
    }
}
