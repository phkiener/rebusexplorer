namespace RebusExplorer.App.Launcher
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using MediatR;

    public partial class Launcher
    {
        public Launcher(IMediator mediator)
        {
            this.mediator = mediator;
            DataContext = new LauncherViewModel();

            InitializeComponent();
        }

        private LauncherViewModel ViewModel => DataContext as LauncherViewModel;

        private async void InitializeHistory(object sender, EventArgs e)
        {
            var connections = await LoadRecentConnections();

            ViewModel.RememberedConnections.Clear();
            foreach (var connection in connections)
            {
                ViewModel.RememberedConnections.Add(connection);
            }
        }

        private async void Connect(object sender, RoutedEventArgs e)
        {
            var canConnect = CheckConnection(ViewModel.CurrentConnection);
            ViewModel.IsBusy = true;

            if (!await canConnect)
            {
                MessageBox.Show(this, "Cannot connect to the Rebus SQL server", "Connection failed", MessageBoxButton.OK);

                ViewModel.IsBusy = false;
                return;
            }

            var mainWindow = App.CreateWindow<MainWindow.MainWindow>();
            mainWindow.SetConnectionInfo(ViewModel.CurrentConnection.ConnectionString, ViewModel.CurrentConnection.QueueName);
            mainWindow.Show();

            await RememberConnection(ViewModel.CurrentConnection);
            ViewModel.IsBusy = false;
            Close();
        }

        private void SelectConnectionFromHistory(object sender, SelectionChangedEventArgs e)
        {
            if (ViewModel.SelectedConnection != null)
            {
                ViewModel.CurrentConnection.ConnectionString = ViewModel.SelectedConnection.ConnectionString;
                ViewModel.CurrentConnection.QueueName = ViewModel.SelectedConnection.QueueName;
            }
        }
    }
}
