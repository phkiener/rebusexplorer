namespace RebusExplorer.Contracts
{
    public sealed class Connection
    {
        public enum ConnectionType
        {
            SqlDirect
        }

        public Connection(string connectionString, string queueName, ConnectionType connectionType)
        {
            ConnectionString = connectionString;
            QueueName = queueName;
            Type = connectionType;
        }

        public string ConnectionString { get; }
        public string QueueName { get; }
        public ConnectionType Type { get; }
    }
}
