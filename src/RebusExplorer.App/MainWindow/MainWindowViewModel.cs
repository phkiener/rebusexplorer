namespace RebusExplorer.App.MainWindow
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        private readonly ObservableCollection<MessageHeaderViewModel> messageHeaders;
        private readonly ObservableCollection<MessageViewModel> messages;
        private bool allowMessageOperations;
        private string messageBody;
        private MessageViewModel selectedMessage;

        public MainWindowViewModel()
        {
            messages = new ObservableCollection<MessageViewModel>();
            messageHeaders = new ObservableCollection<MessageHeaderViewModel>();
            selectedMessage = null;
            messageBody = string.Empty;
            allowMessageOperations = false;
        }

        public ObservableCollection<MessageViewModel> Messages => messages;
        public ObservableCollection<MessageHeaderViewModel> MessageHeaders => messageHeaders;

        public MessageViewModel SelectedMessage
        {
            get => selectedMessage;
            set
            {
                selectedMessage = value;
                PropertyHasChanged(nameof(SelectedMessage));
            }
        }

        public string MessageBody
        {
            get => messageBody;
            set
            {
                messageBody = value;
                PropertyHasChanged(nameof(MessageBody));
            }
        }

        public bool AllowMessageOperations
        {
            get => allowMessageOperations;
            set
            {
                allowMessageOperations = value;
                PropertyHasChanged(nameof(AllowMessageOperations));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void PropertyHasChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
