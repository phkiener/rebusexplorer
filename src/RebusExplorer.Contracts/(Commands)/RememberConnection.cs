namespace RebusExplorer.Contracts
{
    using MediatR;

    public sealed class RememberConnection : IRequest
    {
        public RememberConnection(Connection connection)
        {
            Connection = connection;
        }

        public Connection Connection { get; }
    }
}
