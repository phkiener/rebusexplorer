﻿namespace RebusExplorer.App.MainWindow
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using MediatR;

    public partial class MainWindow
    {
        public MainWindow(IMediator mediator)
        {
            this.mediator = mediator;
            DataContext = new MainWindowViewModel();

            InitializeComponent();
            ConnectionChanged += RefreshMessages;
            ConnectionChanged += (_, __) => Title = $"RebusExplorer - Connected to {currentConnection.QueueName}";
        }

        private MainWindowViewModel ViewModel => DataContext as MainWindowViewModel;

        private event EventHandler ConnectionChanged;

        private async void RefreshMessages(object sender, EventArgs eventArgs)
        {
            try
            {
                var messages = await GetMessages();

                ViewModel.Messages.Clear();
                foreach (var message in messages)
                {
                    ViewModel.Messages.Add(message);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    owner: this,
                    messageBoxText: exception.Message,
                    caption: "Failed to refresh messages",
                    button: MessageBoxButton.OK);
            }
        }

        private void ChangeSelectedMessage(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.MessageHeaders.Clear();
            ViewModel.AllowMessageOperations = ViewModel.SelectedMessage != null;
            ViewModel.MessageBody = ViewModel.SelectedMessage?.Body ?? string.Empty;

            if (ViewModel.SelectedMessage != null)
            {
                foreach (var header in ViewModel.SelectedMessage?.Headers)
                {
                    ViewModel.MessageHeaders.Add(header);
                }
            }
        }

        private async void RequeueSelectedMessage(object sender, RoutedEventArgs e)
        {
            try
            {
                await RequeueMessage(ViewModel.SelectedMessage.MessageId, ViewModel.MessageBody);
                ViewModel.Messages.Remove(ViewModel.SelectedMessage);
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    owner: this,
                    messageBoxText: exception.Message,
                    caption: "Failed to requeue messages",
                    button: MessageBoxButton.OK);
            }
        }

        private async void DropSelectedMessage(object sender, RoutedEventArgs e)
        {
            try
            {
                await DropMessage(ViewModel.SelectedMessage.MessageId);
                ViewModel.Messages.Remove(ViewModel.SelectedMessage);
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    owner: this,
                    messageBoxText: exception.Message,
                    caption: "Failed to drop messages",
                    button: MessageBoxButton.OK);
            }
        }
    }
}
