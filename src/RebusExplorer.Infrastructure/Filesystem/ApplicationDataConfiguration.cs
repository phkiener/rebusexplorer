namespace RebusExplorer.Infrastructure.Filesystem
{
    public sealed class ApplicationDataConfiguration
    {
        public ApplicationDataConfiguration(string rememberedConnectionsFilePath, int rememberedConnectionsCount)
        {
            RememberedConnectionsFilePath = rememberedConnectionsFilePath;
            RememberedConnectionsCount = rememberedConnectionsCount;
        }

        public string RememberedConnectionsFilePath { get; }
        public int RememberedConnectionsCount { get; }
    }
}
