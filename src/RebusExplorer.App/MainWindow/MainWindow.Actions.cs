namespace RebusExplorer.App.MainWindow
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Common;
    using Contracts;
    using MediatR;
    using Newtonsoft.Json;

    public partial class MainWindow
    {
        private readonly IMediator mediator;
        private Connection currentConnection;

        public void SetConnectionInfo(string connectionString, string queueName)
        {
            currentConnection = new Connection(connectionString, queueName, Connection.ConnectionType.SqlDirect);
            ConnectionChanged?.Invoke(this, EventArgs.Empty);
        }

        private async Task<IReadOnlyList<MessageViewModel>> GetMessages()
        {
            var messages = await mediator.SendInNewThreadAsync(new GetMessages(currentConnection));

            return messages.Select(MapToViewModel).ToList();
        }

        private async Task RequeueMessage(long messageId, string editedBody)
        {
            var command = new EditAndRequeueMessage(messageId, editedBody, currentConnection);
            await mediator.Send(command);
        }

        private async Task DropMessage(long messageId)
        {
            var command = new DropMessage(messageId, currentConnection);
            await mediator.Send(command);
        }

        private static MessageViewModel MapToViewModel(Message message)
        {
            return new MessageViewModel(
                messageId: message.Id,
                body: PrettifyJson(message.Body),
                messageTitle: $"{message.Type} ({message.Id})",
                headers: message.Headers.Select(h => new MessageHeaderViewModel(h.Name, h.Value)).ToList());
        }

        private static string PrettifyJson(string json)
        {
            using var reader = new StringReader(json);
            using var writer = new StringWriter();

            var jsonReader = new JsonTextReader(reader);
            var jsonWriter = new JsonTextWriter(writer) { Formatting = Formatting.Indented };

            jsonWriter.WriteToken(jsonReader);
            return writer.ToString();
        }
    }
}
