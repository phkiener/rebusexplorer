namespace RebusExplorer.Contracts
{
    using MediatR;

    public sealed class CanConnect : IRequest<bool>
    {
        public CanConnect(Connection connection)
        {
            Connection = connection;
        }

        public Connection Connection { get; }
    }
}
